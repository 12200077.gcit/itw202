import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import ButtonExample from './components/ButtonExample';
import TouchableHighlightExample from './components/ButtonExample';
import TouchableNativeFeedbackExample from './components/ButtonExample';
import TouchableOpacityExample from './components/ButtonExample';
import TouchableWithoutFeedbackExample from './components/ButtonExample';
import CustomComponents from './components/CustomComponents';


export default function App() {
  return (
    // <ButtonExample></ButtonExample>
    // <TouchableHighlightExample></TouchableHighlightExample>
    // <TouchableNativeFeedbackExample></TouchableNativeFeedbackExample>
    // <TouchableOpacityExample></TouchableOpacityExample>
    <CustomComponents>
    
    </CustomComponents>
  
  );
}


