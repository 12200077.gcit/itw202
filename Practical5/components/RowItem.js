import  React from 'react'
import {TouchableOpacity, Text, StyleSheet, View} from 'react-native';
import { COLORS } from '../constants/Colors';
export const RowItem = ({text})=>{
    return (
        <TouchableOpacity style={styles.row}>
            <Text style={styles.text}>{text}</Text>
        </TouchableOpacity>
    )
}
const styles = StyleSheet.create({
    row:{
        paddingHorizontal:20,
        paddingVertical: 16,
        justifyContent:"space-between",
        alignItems:"center",
        flexDirection:"row",
        backgroundColor: COLORS.white,
    },
    title:{
        color:COLORS.text,
        fontSize:16,

    }
})