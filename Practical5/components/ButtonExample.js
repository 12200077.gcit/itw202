import React, { useState} from 'react';
import { View, Text, Button,TouchableWithoutFeedback, TouchableOpacity, StyleSheet,TouchableNativeFeedback, TouchableHighlight} from 'react-native';
const ButtonExample = ()=>{
    const [count, setCount] = useState(0);
    return(
        <View style={styles.container}>
<Text>You clicked {count} times</Text>
<Button
onPress={()=> setCount (count+1)}
title="Count"/>
</View>
        
    )
};


const  TouchableHighlightExample =()=>{
  const [count, setCount] = useState(0);
  return(
    <View style={styles.container}>
      <Text>You clicked {count} times</Text>
      <TouchableHighlight style={styles.button}
      onPress={()=> setCount(count + 1)}>
        <Text>Count</Text>
        </TouchableHighlight> 

    </View>
  )
}

const  TouchableNativeFeedbackExample =()=>{
  const [count, setCount] = useState(0);
  return(
    <View style={styles.container}>
      <Text>You clicked {count} times</Text>
      <TouchableNativeFeedback style={styles.button}
      onPress={()=> setCount(count + 1)}>
        <Text>Count</Text>
        </TouchableNativeFeedback> 

    </View>
  )
}
const  TouchableOpacityExample =()=>{
  const [count, setCount] = useState(0);
  return(
    <View style={styles.container}>
      <Text>You clicked {count} times</Text>
      <TouchableOpacity style={styles.button}
      onPress={()=> setCount(count + 1)}>
        <Text>Count</Text>
        </TouchableOpacity> 

    </View>
  )
}

const  TouchableWithoutFeedbackExample =()=>{
  const [count, setCount] = useState(0);
  return(
    <View style={styles.container}>
      <Text>You clicked {count} times</Text>
      <TouchableWithoutFeedback style={styles.button}
      onPress={()=> setCount(count + 1)}>
        <Text>Count</Text>
        </TouchableWithoutFeedback> 

    </View>
  )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    button:{
      alignItems:"center",
      backgroundColor: "#DDDDDD",
      padding: 10

    }
  });
  // export default ButtonExample;
  // export default TouchableHighlightExample ;
  // export default TouchableNativeFeedbackExample;
  // export default TouchableOpacityExample;
  export default TouchableWithoutFeedbackExample;


