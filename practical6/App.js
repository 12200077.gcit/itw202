import { StyleSheet,ScrollView, Text, View, Button, TextInput,FlatList } from 'react-native';
import React, {useState} from 'react';

import AspirationInput from './components/AspirationInput';
import { AspirationItem } from './components/AspirationItem';

export default function App() {
  const [courseAspirations, setCourseAspirations] = useState([]);

  const addAspirationHandler=(aspirationTitle)=>{
    // console.log(enteredAspiration);
    // setCourseAspirations([...courseAspirations, enteredAspiration]);
    setCourseAspirations(currentAspirations =>[
      ...currentAspirations,
      {key: Math.random().toString(), value: aspirationTitle}]);
  };
  return (
    <View style= {styles.screen}>
     <AspirationInput onAddAspiration = {addAspirationHandler}/>
      <FlatList
      data ={courseAspirations}
      renderItem={itemData=>
      <AspirationItem title = {itemData.item.value}  />
      }
      />
     
     
    </View>
  )
}

const styles = StyleSheet.create({
  screen:{
    padding : 50,
  },
});