import React, {useState} from "react"
import {StyleSheet, View, TextInput, Button} from "react-native"

 const AspirationInput = (props) => {
    const [enteredAspiaration, setEnteredAspiration]=useState('');
    const AspirationInputHandler=(enteredText)=>{
        setEnteredAspiration(enteredText);
    }
    return(
        <View style={styles.inputContainer}>
            <TextInput
            placeholder="My Asporation from this module"
            style ={styles.input}
            onChangeText={AspirationInputHandler}
            value={enteredAspiaration}
            />
            <Button
            title ="ADD"
            onPress={()=> props.onAddAspiration(enteredAspiaration)}
            />
        </View>
    )
}
export default AspirationInput;

const styles = StyleSheet.create({
    inputContainer:{
        flexDirection:"row",
        justifyContent: 'space-between',
        alignItems:"center"
    },
    input:{
        width:'80%',
        borderColor:"black",
        borderWidth: 1,
        padding:10
         
    }
})