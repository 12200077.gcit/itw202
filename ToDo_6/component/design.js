
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const MyDesign = () => {
    return(
        <View style={styles.masterStyle}>
        <View style={styles.square1}><Text style={styles.square1}>1</Text></View>
        <View style={styles.square2}><Text style={styles.square2}>2</Text></View>
        <View style={styles.square3}><Text style={styles.square3}>3</Text></View>
        </View>

    );
}
const styles = StyleSheet.create({
    masterStyle:{
        flexDirection: "row",
        flex: 1,
        backgroundColor: '#fff',
        marginBottom:600,
        marginRight:150,
        marginTop:50,
        marginLeft:50,
        justifyContent:"center",
        alignItems: "center"
        
    },
    square1:{
        backgroundColor: 'red',
        flex: 3,
        textAlign:"center",
        textAlignVertical:"center",
        
    
       
      },
      square2:{
        backgroundColor: 'blue',
        flex: 6,
        textAlign:"center",
        textAlignVertical:"center"

      },
      square3:{
        backgroundColor: 'green',
        flex: 1,
        textAlign:"center",
        textAlignVertical:"center"
        
       
      }

}) 
export default MyDesign;