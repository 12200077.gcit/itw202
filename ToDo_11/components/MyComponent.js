import React, { useState} from "react";
import { Text,View,StyleSheet, Button } from "react-native";

const MyComponent=()=>{
    const [count, setCount] = useState(1);
    const [text, setText]= useState("The Button isn't pressed yet")

    return(
        <View style={styles.container}>
            <Text> {text}</Text>
                <Button
                onPress={()=>{ setCount (count+1)
                  setText("The button was pressed " + count +" times!")}}
                disabled={count>3}
                title="PRESS ME" 
               />
      </View>
    )
}
export default MyComponent;

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });