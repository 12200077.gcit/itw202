import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import {add, multiply} from './Component/nameExport';
import divide from './Component/defaultExport';
export default function App() {
  return (
    <View style={styles.container}>
      
      <Text>Result of Addition: {add(5, 6)}</Text>
      <Text>Result of Multiplication: {multiply(5, 8)}</Text>
      <Text>Result of Division: {divide(10, 2)}</Text>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
