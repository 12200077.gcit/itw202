import { firebase } from '@firebase/app'

const CONFIG = {
  apiKey: "AIzaSyDAjOorU6g5nqYkaB6nceLU_lKGqhRhWP0",
  authDomain: "my-app-3c97a.firebaseapp.com",
  databaseURL: "https://my-app-3c97a-default-rtdb.firebaseio.com",
  projectId: "my-app-3c97a",
  storageBucket: "my-app-3c97a.appspot.com",
  messagingSenderId: "102024882168",
  appId: "1:102024882168:web:e1fc36d9153a3c6b12580d"
};
firebase.initializeApp(CONFIG)

export default firebase;
