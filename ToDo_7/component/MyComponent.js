import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
 const MyComponent=()=>{
     return(
         <View style={styles.container1}>
             <View style={styles.box1}/> 
             <View style={styles.box2}/>           
         </View>
     )
 }
 export default MyComponent

 const styles= StyleSheet.create({
     container1:{
         flex:1,
         backgroundColor:"white",
         justifyContent:"center",
         alignItems:"center", 
         marginTop:400,
     },
     box1:{
        backgroundColor: 'red',
        
       flex:0.3,
        width:150,
     },
     box2:{
        backgroundColor: 'blue',
       flex:0.3,
        width:150,      
     }

 })

 