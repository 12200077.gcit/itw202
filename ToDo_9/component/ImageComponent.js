import React from "react";
import {Text, View, StyleSheet, Image} from 'react-native'

const ImageComponent=()=>{
    return(
        <View style={styles.container}>
            <Image
            source={{uri: 'https://picsum.photos/100/100' }}
            style={styles.style1}/>
            <Image
            source={require( '../assets/react-native.png')}
            style={styles.style2}/>       
        </View>
    )  
}
export default ImageComponent;
const styles = StyleSheet.create({
    container:{
        backgroundColor:"#fff",
        flex:1,
        justifyContent: "center",
        alignItems:"center",
    },
    
    style1:{
        width: 100,
         height: 100,
        
    },
    style2:{
        width: 100,
        height: 100,
        
    }
})