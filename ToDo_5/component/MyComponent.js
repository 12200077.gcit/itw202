import React from "react";
import {View, StyleSheet, Text} from "react-native";

 const MyDetails = () => {

const greet = 'Sangay Tshering';

    return (
        <View>
            <Text style = {styles.style1}>Getting started with react native!</Text>
            <Text style = {styles.style2}>My name is {greet}</Text>
        
        </View>
    )
};


const styles = StyleSheet.create({
    style1:{
        fontSize: 45
    },
    style2:{
        fontSize: 20

    }
    
});
export default MyDetails;