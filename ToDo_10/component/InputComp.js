import React,  {useState} from "react";
import {View, Text,TextInput,StyleSheet} from "react-native";

const MyComp=()=>{
   
    
    const [input, setInput] = useState("");
    console.log({input})
    
    return(
        <View style={styles.container}>
            <Text>what is your name?</Text>
            
            <TextInput 
            style={styles.input}
            placeholder = "enter your name here"
            onChangeText={(text)=> setInput(text)} 
            secureTextEntry={true}
            />
            <Text>Hi {input} from Gyelpozhing college of Information Technology</Text>   
           
        </View>
    );
    
}

export default MyComp;

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    input:{
        borderColor:"grey",
         borderWidth:1,
         width:200,
         borderRadius:20,
         padding:5
       

    },
  });