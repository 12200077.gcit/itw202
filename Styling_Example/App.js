import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import styles from './component/styles.js';
export default function App() {
 
  return (
    <View style={styles.container}>
      <Text style = {styles.welcome }>Welcome to React Native!</Text>
      <Text />
    </View>
  );
}


