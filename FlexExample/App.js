import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    
    <View style={styles.container}>
    <View style={styles.square}/>
    <View style={styles.square}/>
    <View style={styles.square}/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#7CA1B4',
    justifyContent: 'center',
    alignItems:"flex-end"
  },
  square:{
 
    backgroundColor: '#7cb48f',
    width: 100,
    borderWidth: 1,
    borderColor: "goldenrod",
    height: 100,
    margin:4
    
   
  }
});
